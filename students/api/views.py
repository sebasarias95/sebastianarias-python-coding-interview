from rest_framework import viewsets
from rest_framework import permissions

from serializers import StudersSerializer
from ..migrations import Students


class StudentsView(viewsets.ModelViewSet):
    queryset = Students.objects.all()
    serializer_class = StudentsSerializer