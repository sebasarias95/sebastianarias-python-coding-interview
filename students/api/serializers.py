from rest_framework import serializers

from ..models import Students


class StudersSerializer(serializers.HyperlinkModelSerializer):
    class Meta:
        model = Students
        fields = ['student_id', 'enrollment_complete', 'courses']
