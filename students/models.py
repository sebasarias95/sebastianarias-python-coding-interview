from django.db import models


class Students(models.Model):
    student_id = models.IntegerField(primary_key=True)
    enrollment_complete = models.BooleanField()
    courses = models.ManyToManyField('subjects.Subject')

    def complete_flag(self, minimum_courses=7):
        return len(self.objects.all()) > minimum_courses
